import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { WeatherComponent } from './weather.component';
import { HttpService } from 'src/app/services/httprequest.service';
import { Router, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { CityDetailsComponent } from '../city-details/city-details.component';

describe('WeatherComponent', () => {
  let component: WeatherComponent;
  let fixture: ComponentFixture<WeatherComponent>;
  let router: Router;
  let routes: Routes;
  let location: Location;

  beforeEach(async () => {

    routes = [{ path: '', redirectTo: '/weather', pathMatch: 'full' },
    { path: 'weather', component: WeatherComponent },
    { path: 'details', component: CityDetailsComponent },]

    await TestBed.configureTestingModule({
      declarations: [WeatherComponent],
      imports: [
        BrowserModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [{ provide: HttpService, useClass: HttpService },
      ]
    })
      .compileComponents();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(WeatherComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    router.initialNavigation();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('on click of any weather widget, navigate to its city details component', () => {
    component.navigateCityDetails('london');
    router.navigate(["details"], {
      queryParams: { city: "london" }
    }).then(() => {
      expect(location.path()).toBe('weather');
    });
  });

  it('should call get_Data method of service, on call of  getWeatherData() ', fakeAsync(() => {
    const service = fixture.debugElement.injector.get(HttpService);
    let spy_getPosts = spyOn(service, "get_Data").and.callFake(() => {
      return of(
        {
          name: "",
          weather: [{ icon: "" }],
          main: { temp: "" },
          sys: {
            sunrise: "",
            sunset: ""
          }
        }
      ).pipe(delay(100));
    });

    component.getWeatherData('london');
    tick(100);
    expect(service.get_Data).toHaveBeenCalled();
  }))

});
