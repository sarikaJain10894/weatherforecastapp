import { Component, OnInit } from '@angular/core';
import { weatherModel } from '../../models/weather.model';
import { HttpService } from 'src/app/services/httprequest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  
  cityNames = ['London', 'Moscow', 'Rome', 'Paris', 'Budapest']; // 5 European cities names
  WeatherData: weatherModel[] = [];
  weatherSubs: any;
  constructor(private httpservice: HttpService, private router: Router) { }

  ngOnInit() {
    //get weather data for each city:: can be replaced with single API call if any API returns data for multiple cities
    this.cityNames.forEach(city => {
      this.getWeatherData(city);
    });
  }

  //http call from service to fetch data
  getWeatherData(cityName: string) {
    this.weatherSubs = this.httpservice.get_Data(cityName, 'weather').subscribe(data => {
      this.populateWeatherData(data);
    });
  }

  //populate data as per our requirement
  populateWeatherData(data: any) {
    let citydetails: weatherModel = new weatherModel();
    let sunriseTime = new Date(data.sys.sunrise * 1000);
    let sunsetTime = new Date(data.sys.sunset * 1000);
    citydetails.cityName = data.name;
    citydetails.sunriseTime = sunriseTime.toLocaleTimeString();
    citydetails.imageUrl = `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`
    citydetails.sunsetTime = sunsetTime.toLocaleTimeString();
    citydetails.temperature = (data.main.temp - 273.15).toFixed(0);

    this.WeatherData.push(citydetails)
  }

  //navigate to city details page on click of any city widget with queryparameter as city name
  navigateCityDetails(name: string) {
    this.router.navigate(['details'], {
      queryParams: { city: name }
    });
  }

  
  ngOndestroy() {
    if (this.weatherSubs) this.weatherSubs.unsubscribe(); //unsubscribe all subscriptions
  }
}
