import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/httprequest.service';
import { ActivatedRoute } from '@angular/router';
import { foreCastDetails } from '../../models/forecastDetails.model';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
export class CityDetailsComponent implements OnInit {

  nDaysData: foreCastDetails[] = [];
  cityName: string = ''
  weatherforcastPredictionSubs: any;
  time = "09:00:00"
  constructor(private activeRoute: ActivatedRoute, private httpservice: HttpService) { }

  ngOnInit(): void {
    this.cityName = this.activeRoute.snapshot.queryParams.city;
    this.getNdaysWeatherForecast(this.cityName); // on page initialise fetch city weather forecast details 
  }

  //make http reuest call from service
  getNdaysWeatherForecast(cityName: string) {
    this.weatherforcastPredictionSubs = this.httpservice.get_Data(cityName, 'forecast').subscribe(data => {
      this.populateData(data);
    });
  }

  //populate required data from response object
  populateData(data: any) {
    this.cityName = data.city.name;
    
    //filter weather data at  9:00:00
    let filteredData = data.list.filter((day: any) => day.dt_txt.includes(this.time)); 

    filteredData.forEach((dt: any) => {
      let foreCastDetail = new foreCastDetails();
      foreCastDetail.temperature = (dt.main.temp - 273.15).toFixed(0);
      foreCastDetail.sea_level = dt.main.sea_level;
      foreCastDetail.date = dt.dt_txt;
      foreCastDetail.iconDetails = dt.weather[0];
      foreCastDetail.imageUrl = `http://openweathermap.org/img/wn/${dt.weather[0].icon}@2x.png`

      this.nDaysData.push(foreCastDetail);
    });
  }

  ngOndestroy() {
    if (this.weatherforcastPredictionSubs) this.weatherforcastPredictionSubs.unsubscribe();
  }

}
