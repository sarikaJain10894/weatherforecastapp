import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { CityDetailsComponent } from './city-details.component';
import { HttpService } from 'src/app/services/httprequest.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { WeatherComponent } from '../weather/weather.component';

describe('CityDetailsComponent', () => {
  let component: CityDetailsComponent;
  let fixture: ComponentFixture<CityDetailsComponent>;
  let activatedRoute: ActivatedRoute
  let router: Router;
  let routes: Routes;

  beforeEach(async () => {
    routes = [{ path: '', redirectTo: '/weather', pathMatch: 'full' },
    { path: 'weather', component: WeatherComponent }]
    await TestBed.configureTestingModule({
      declarations: [CityDetailsComponent],
      imports: [
        BrowserModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        { provide: HttpService, useClass: HttpService },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                city: 'london'
              }
            }
          }
        }]
    })
      .compileComponents();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(CityDetailsComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    router.initialNavigation();
    activatedRoute = TestBed.get(ActivatedRoute);
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get city name from activated route', () => {
    activatedRoute.snapshot.queryParams.city = 'london';
    fixture = TestBed.createComponent(CityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.cityName).toBe('london');
  });

  it('Oninit should  call getNdaysWeatherForecast()', () => {
    spyOn(component, 'getNdaysWeatherForecast');
    component.ngOnInit();
    expect(component.getNdaysWeatherForecast).toHaveBeenCalled();
  });

});
