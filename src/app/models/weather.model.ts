export class weatherModel {
    cityName: string = '';
    temperature: string = '';
    sunriseTime: string = '';
    sunsetTime: string = '';
    imageUrl: string = '';
}