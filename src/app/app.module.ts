import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './Components/weather/weather.component';
import { CityDetailsComponent } from './Components/city-details/city-details.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { HttpInterceptorInterceptor } from './services/http-interceptor.interceptor';
@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    CityDetailsComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
