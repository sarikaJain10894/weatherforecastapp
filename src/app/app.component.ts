import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularApplication';
  constructor(private router: Router) { }

  //on click of home icon, navigate to weather/main page
  gotoWeatherPage() {
    this.router.navigate(['weather'])
  }
}
