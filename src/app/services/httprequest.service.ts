import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl = `https://api.openweathermap.org/data/2.5/`;
  apiKey = '&appid=3d8b309701a13f65b660fa2c64cdc517'

  constructor(public http: HttpClient) { }

  get_Data(
    city: string,
    url: string
  ): Observable<any> {
    const headers = new HttpHeaders({
      Accept: 'application/json'
    });

    //form request url based on params
    let reqURL = `${this.baseUrl}${url}?q=${city}${this.apiKey}`;
    
    return this.http
      .get(reqURL, { headers: headers }).pipe(map((res) => {
        return res;
      }));
  }
}
